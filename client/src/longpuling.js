import React from "react";
import axios from "axios";
import { response } from "express";

const Longpuling = () => {
  const [message, setMessage] = React.useState([]);
  const [value, setValue] = React.useState("");

  React.useEffect(() => {
    subscribe();
  }, []);

  const subscribe = async () => {
    axios
      .get("http://localhost:5000/new-message")
      .then((response) => setMessage(response))
      .catch((error) => console.error(error));
  };

  const sendMessage = async () => {
    await axios.post("http://localhost:5000/new-message", {
      message: value,
      id: Date.now(),
    });
  };

  return (
    <>
      <div className="form">
        <input
          value={value}
          onChange={(e) => setValue(e.target.value)}
          className="input"
          type="text"
          placeholder="Введите сообщение"
        />
        <button className="button" onClick={sendMessage}>
          отправить
        </button>
      </div>
      <div className="form form2">
        <div>{message}</div>
      </div>
    </>
  );
};

export default Longpuling;
