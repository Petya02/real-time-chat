import React from "react";

const Socket = () => {
  const [message, setMessage] = React.useState([]);
  const [value, setValue] = React.useState("");
  const socket = React.useRef()
  const [connected, setConnected] = React.useState(false)
  const [username, setUsername] = React.useState('')

  const connect = () => {
    socket.current = new WebSocket('ws://localhost:5000')

    socket.current.onopen = () => {
      setConnected(true)
      const message = {
        event: 'connection',
        username,
        id: Date.now()
      }
      socket.current.send(JSON.stringify(message))
    }

    socket.current.onmessage = (event) => {
      const message = JSON.parse(event.data)
      setMessage(prev => [message, ...prev])
    }

    socket.current.onclose = () => {
      console.log("socket close");
    }

    socket.current.onerror = () => {
      console.log("socket error");
    }
  }

  const sendMessage = async () => {
    const message = {
      username,
      message: value,
      id: Date.now(),
      event: 'message'
    }

    socket.current.send(JSON.stringify(message))
    setValue('')
  };

  if (!connected) {
    return (
      <div className="form">
        <input value={username} onChange={e => setUsername(e.target.value)} className="input" placeholder="введите ваше имя" />
        <button onClick={connect} className="button">войти</button>
      </div>
    )
  }

  return (
    <>
      <div className="form">
        <input
          value={value}
          onChange={(e) => setValue(e.target.value)}
          className="input"
          type="text"
          placeholder="Введите сообщение"
        />
        <button className="button" onClick={sendMessage}>
          отправить
        </button>
      </div>
      <div className="form form2">
        {message.map(mess => {
          console.log(mess);
          return <div key={mess.id}>
            {mess.username}. {mess.message}
          </div>
        })}
      </div>
    </>
  );
};

export default Socket;
