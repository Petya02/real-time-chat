import Websocket from "./websocket";

function App() {
  return (
    <div>
      <Websocket />
    </div>
  );
}

export default App;
